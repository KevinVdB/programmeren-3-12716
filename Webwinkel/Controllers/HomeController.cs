﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Webwinkel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Mikmak Webwinkel";
            return View();
        }

        public ActionResult AdminIndex()
        {
            ViewBag.Title = "Mikmak beheer";
            // We gaan ervan uit dat de database bestaat
            ViewBag.Feedback = "Database Webwinkel is al gemaakt.";
            // Kijk of de database al bestaat. Indien niet, maak de database.
            Models.Dal Dal = new Models.Dal();
            if (!Dal.Database.Exists())
            {
                try
                {
                    Dal.Database.Create();
                    ViewBag.Feedback = "Database Webwinkel is gemaakt!";
                }
                catch (Exception e)
                {
                    ViewBag.Feedback = e.Message;
                }
            }
            return View();
        }
    }
}