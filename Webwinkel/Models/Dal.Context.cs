﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Webwinkel.Models
{
    public class Dal : DbContext
    {
        public Dal() : base("name=WebwinkelWindowsAuthentication")
            {

        }

        public virtual DbSet<UnitBase> DbSetUnitBase { get; set; }
    }
}